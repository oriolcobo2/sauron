# Sauron

## Sauron is a telegram bot created with Spring Boot and java 1.8

* **Create** _Application.yaml_ **inside of** _src/main/resources/_

```sh
# this is the content that it has to have:
bot:
  token: {$here your token Generated with @BotFather}
  username: {$here your bot name Generated with @BotFather (ending in _bot)}
```

* [Check the Telegram API is you want to contribute!](https://core.telegram.org/bots/api)