package main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;

/*
@author Oriol Cobo
@description This bot is under development.
*/
@Component
public class SauronBot extends TelegramLongPollingBot {
	
	private static final Logger logger = LoggerFactory.getLogger(SauronBot.class);
	
	@Value("${bot.token}")
	private String token;
	
	@Value("${bot.username}")
	private String username;
	
	@Override
	public String getBotToken() {
		return token;
	}
	
	@Override
	public String getBotUsername() {
		return username;
	}
	
	@Override
	public void onUpdateReceived(Update update) {
		if (update.hasMessage()) {
			Message message = update.getMessage();
			SendMessage response = new SendMessage();
			Long chatId = message.getChatId();
			response.setChatId(chatId);
			String text = message.getText().toLowerCase();
			if (text.contains("nieto")) {
				try {
					response.setText("Huele a pike aqui...");
					execute(response);
					logger.info("Sent message \"{}\" to {}", text, chatId);
				} catch (TelegramApiException e) {
					logger.error("Failed to send message \"{}\" to {} due to error: {}", text, chatId, e.getMessage());
				}
			}
			
		}
	}

	@PostConstruct
	public void start() {
		logger.info("username: {}, token: {}", username, token);
	}

}